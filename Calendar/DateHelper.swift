//
//  DateHelper.swift
//  Calendar
//
//  Created by Son Dang on 1/5/17.
//  Copyright © 2017 Son Dang. All rights reserved.
//

import UIKit

class DateHelper: NSObject {
    static func getTime(time: Date, calendar: Calendar) -> (Int, Int, Int, Int, Int) {
        let year = calendar.component(.year, from: time)
        let month = calendar.component(.month, from: time)
        let day = calendar.component(.day, from: time)
        let weekday = calendar.component(.weekday, from: time)
        let weekOfMonth = calendar.component(.weekOfMonth, from: time)
        
        // 1 is sunday, 2 is monday
        return (weekday, day, weekOfMonth, month, year)
    }
    
    static func before(month Month: Int, year Year: Int) -> (Int, Int) {
        return DateHelper.moveMonth(from: Month, year: Year, step: -1)
    }
    
    static func after(month Month: Int, year Year: Int) -> (Int, Int) {
        return DateHelper.moveMonth(from: Month, year: Year, step: 1)
    }
    
    static func moveMonth(from Month: Int, year Year: Int, step Step: Int) -> (Int, Int) {
        
        var demoM = (Month + Step) % 12
        var demoY = (Month + Step) / 12
        
        if demoM <= 0 {
            if Month + Step <= 0 {
                demoM = 12 + (Month + Step)%12
            }
            else {
                demoM = 12
            }
        }
        
        if Month + Step <= 0 {
            demoY -= 1
        }
        
        return (demoM, Year+demoY)
    }
}
