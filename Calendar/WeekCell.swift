//
//  WeekCell.swift
//  Calendar
//
//  Created by Son Dang on 1/5/17.
//  Copyright © 2017 Son Dang. All rights reserved.
//

import UIKit

class WeekCell: UITableViewCell {

    private var buttonArr: [UIButton] = []
    private let dateWith: Float = CalendarView.viewWidth/7
    private let dateHeight: Float = CalendarView.viewHeight/6
    
    @IBOutlet weak var vcCell: UIView!;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.createButtons()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellWithCalendar(month Month:Int, year Year:Int) {
        let calendar = Calendar.current
        let strTime = "01-" + String(Month) + "-" + String(Year) + " +0000"
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy Z"
        
        let focusDate: Date = formatter.date(from: strTime)!
        let dayInWeek: Int = DateHelper.getTime(time: focusDate, calendar: calendar).0
        let range = calendar.range(of: .day, in: .month, for: focusDate)!
        let numDays = range.count
        
        var countDate: Int = 1
        for index in 0 ... 41 {
            let button: UIButton = self.buttonArr[index]
            button.setTitle("", for: .normal)
            if index == dayInWeek-1 {
                button.setTitle(String(countDate), for: .normal)
                countDate += 1
            }
            else if countDate > 1 && countDate < numDays+1 {
                button.setTitle(String(countDate), for: .normal)
                countDate += 1
            }
        }
    }
    
    //MARK: support methods
    
    private func frameForPosition(x X:Int) -> CGRect {
        let x: CGFloat!
        let y: CGFloat
        if X < 7 {
            x = CGFloat(dateWith) * CGFloat(X)
            y = 0
        }
        else {
            x = CGFloat(dateWith) * CGFloat(Int(X%7))
            y = CGFloat(dateHeight) * CGFloat(Int(X/7))
        }
        return CGRect.init(x: x, y: y, width: CGFloat(dateWith), height: CGFloat(dateHeight))
    }
    
    private func createButtons() {
        for indexX in 0 ... 41 {
            let button: UIButton = UIButton.init(frame: self.frameForPosition(x:indexX))
            button.backgroundColor = UIColor.clear
            button.setTitleColor(UIColor.gray, for: .normal)
            button.tag = indexX + 5
            self.addSubview(button)
            self.buttonArr.append(button)
        }
        
        self.styleTheCell()
    }
    
    // support user styling the cell
    private func styleTheCell() {
        for index in 0 ... 41 {
            let button: UIButton = self.buttonArr[index]
            button.titleLabel?.font = button.titleLabel?.font.withSize(14)
        }
    }
}
