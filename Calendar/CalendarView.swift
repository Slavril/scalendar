//
//  CalendarView.swift
//  Calendar
//
//  Created by Son Dang on 1/4/17.
//  Copyright © 2017 Son Dang. All rights reserved.
//

import UIKit

/*
 a calendar has max is 6 rows and always 7 columms
 begin from 1 and ending at 28, 30 or 31
 */

class CalendarView: UIView, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    public static let viewHeight: Float = 250
    public static let viewWidth: Float = 300
    public static let headerHeight: Float = 44
    public static let titleHeight: Float = 64
    
    private let currentDate = Date()
    private let calendar = Calendar.current
    
    var selectedMonth: Int = 0
    var selectedYear: Int = 0
    var selectedIndex: Int = 0
    
    private var selectedDate: Date! // planning
    private var focusDate: Date!
    private var focusMonth: Int!
    private var focusYear: Int!
    private var centerpoint: Int!
    private var calendarSize: Int = 37 // default 5 months
    
    @IBOutlet weak  var headerView: UIView!
    @IBOutlet weak  var titleView: UIView!
    @IBOutlet weak  var yearLabel: UILabel!
    @IBOutlet weak  var monthLabel: UILabel!
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleHeightConstraint: NSLayoutConstraint!
    
    private let dayTitle: [String] = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    private let monthTitle: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    private let monthTitle_full: [String] = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    //MARK: init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        guard Bundle.main.path(forResource: "CalendarView", ofType: "nib") != nil else {
            return
        }
        let nib = UINib.init(nibName: "CalendarView", bundle: nil)
        
        self.contentView = nib.instantiate(withOwner: self, options: nil).last as! UIView
        self.contentView.frame = self.frame
        
        self.addSubview(self.contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        self.setupConstraint()
    }
    
    override init(frame: CGRect) {
        // in construction
        super.init(frame: frame)
        self.contentView = UIView(frame: frame)
        self.addSubview(self.contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.setupConstraint()
        
        self.customInit()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.customInit()
    }
    
    private func setupTitleView() {
        self.titleView.backgroundColor = UIColor.red
        self.titleHeightConstraint.constant = CGFloat(CalendarView.titleHeight)
        
        self.yearLabel.text = "2016"
        self.monthLabel.text = "january"
        
        self.yearLabel.font = self.yearLabel.font.withSize(13)
    }
    
    // create a default header
    private func setupHeaderView() {
        self.headerView.backgroundColor = UIColor.clear
        self.headerHeightConstraint.constant = CGFloat(CalendarView.headerHeight)
        
        for index in 0 ... 6 {
            let label: UILabel = UILabel(frame: CGRect(x: CGFloat(Float(index)*(CalendarView.viewWidth/7)), y: 0, width: CGFloat(CalendarView.viewWidth/7), height: CGFloat(CalendarView.headerHeight)))
            label.text = self.dayTitle[index]
            label.textAlignment = NSTextAlignment.center
            label.font = label.font.withSize(14)

            self.headerView.addSubview(label)
        }
    }
    
    private func customInit() {
        table.register(UINib(nibName: "WeekCell", bundle:Bundle.main), forCellReuseIdentifier: "WeekCell")
        self.setupData()
        
        self.contentWidthConstraint.constant = CGFloat(CalendarView.viewWidth)
        self.contentHeightConstraint.constant = CGFloat(CalendarView.viewHeight)
        
        table.backgroundColor = UIColor.clear
        table.layer.borderWidth = 0.5;
        table.layer.borderColor = UIColor.gray.cgColor
        
        contentView.backgroundColor = UIColor.clear

        table.dataSource = self
        table.delegate = self
        
        let indexPath = NSIndexPath(row: centerpoint, section: 0)
        table.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
        
        self.setupHeaderView()
        self.setupTitleView()
    }
    
    private func getMonth(time: Date) -> Int {
        let month = DateHelper.getTime(time: time, calendar: self.calendar)
        return month.3
    }
    
    private func getYear(time: Date) -> Int {
        let month = DateHelper.getTime(time: time, calendar: self.calendar)
        return month.4
    }
    
    private func setupConstraint() {
        self.addConstraint(self.constraintWithItem(withItem: self, andConstraintToItem: self.contentView, attribute: .top))
        self.addConstraint(self.constraintWithItem(withItem: self, andConstraintToItem: self.contentView, attribute: .bottom))
        self.addConstraint(self.constraintWithItem(withItem: self, andConstraintToItem: self.contentView, attribute: .leading))
        self.addConstraint(self.constraintWithItem(withItem: self, andConstraintToItem: self.contentView, attribute: .trailing))
    }
    
    //MARK: public methods
    
    func setupCalendar(yearCount YearCount: Int, date SetupDate: Date) {
        calendarSize = YearCount*12 + 1
        focusDate = SetupDate
        focusMonth = self.getMonth(time: focusDate)
        focusYear = self.getYear(time: focusDate)
        centerpoint = Int(calendarSize/2)
        table.reloadData()
    }
    
    //MARK: support methods
    
    private func constraintWithItem(withItem item: AnyObject, andConstraintToItem toItem: AnyObject, attribute: NSLayoutAttribute) -> NSLayoutConstraint {
        return NSLayoutConstraint.init(item: item, attribute: attribute, relatedBy: .equal, toItem: toItem, attribute: attribute, multiplier: 1.0, constant: 0.0)
    }
    
    private func setupData() {
        focusDate = Date()
        focusMonth = self.getMonth(time: focusDate)
        focusYear = self.getYear(time: focusDate)
        centerpoint = Int(calendarSize/2)
    }
    
    //MARK: table delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calendarSize
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(CalendarView.viewHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: WeekCell = tableView.dequeueReusableCell(withIdentifier: "WeekCell") as! WeekCell
        
        let stepMonth: (Int, Int)
        if indexPath.row == self.centerpoint {
            cell.updateCellWithCalendar(month: focusMonth, year: focusYear)
            stepMonth = (focusMonth, focusYear)
        }
        else {
            let step: Int = indexPath.row - self.centerpoint
            stepMonth = DateHelper.moveMonth(from: focusMonth, year: focusYear, step: step)
            cell.updateCellWithCalendar(month: stepMonth.0, year: stepMonth.1)
        }
        selectedMonth = stepMonth.0
        selectedYear = stepMonth.1

        //self.monthLabel.text = self.monthTitle_full[selectedMonth-1]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexes = self.table.indexPathsForVisibleRows
        if (indexes?.count)! > 0 {
            if indexes?[0].row == self.centerpoint {
                self.monthLabel.text = self.monthTitle_full[focusMonth-1]
            }
            else {
                let step: Int = (indexes?[0].row)! - self.centerpoint
                let stepMonth = DateHelper.moveMonth(from: focusMonth, year: focusYear, step: step)
                self.monthLabel.text = self.monthTitle_full[stepMonth.0-1]
            }
        }
    }
}
